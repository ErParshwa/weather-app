var express = require('express');
var morgan = require('morgan');
var enforce = require('express-sslify');
var app = express();


app.use(morgan('dev'));
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

app.use(express.static(__dirname));

app.all('/*', function (req, res, next) {
    // Just send the index.html for other files to support HTML5Mode
    res.sendFile('index.html', {
        root: __dirname
    });
});

app.listen(process.env.PORT || 5000);

/*;*/
