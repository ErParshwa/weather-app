angular.module('weatherApp')
    .controller('weatherController', ['weatherService', function (weatherService) {

        //Sea Level time
        this.sea_level_time = "9:00"

        //city object
        this.city = {};

        //ForeCast data & chart values
        this.foreCastData = [];
        this.chartLabels = [];
        this.chartSeries = ['Sea Level'];

        //list of Cities
        this.cityList = [
            { cityName: 'London, UK', id: 2643743 },
            { cityName: 'Paris, France', id: 2988507 },
            { cityName: 'Vienna, Austria', id: 2761369 },
            { cityName: 'Barcelona, Spain', id: 3128760 },
            { cityName: 'Madrid, Spain', id: 3117735 }
        ];

        //get weather data for selected city
        this.getData = (city) => {

            this.isLoading = true;
            this.cityData = {};
            this.foreCastData = [];
            this.chartLabels = [];

            weatherService.getCurrentWeather(city.id).then((data) => {
                //we have data, 
                this.cityData.name = city.cityName;
                this.cityData.id = city.id;
                this.cityData.atmosphere = data.data.weather[0].main;
                this.cityData.icon = "http://openweathermap.org/img/w/" + data.data.weather[0].icon + ".png";
                this.cityData.windSpeed = data.data.wind.speed;
                this.cityData.humidity = data.data.main.humidity;
                this.cityData.sea_level = data.data.main.sea_level;
                this.cityData.temp = data.data.main.temp;
                this.cityData.sunrise = weatherService.getDayAndTime(data.data.sys.sunrise);
                this.cityData.sunset = weatherService.getDayAndTime(data.data.sys.sunset);
            })
            .catch(()=> {
                this.hasError = true;
                alert('Sorry, Something went wrong');
            })
            .finally(()=>{
                this.isLoading = false;
            });
        }

        this.chartDatasetOverride = [
            {
                label: "Sea Level Line chart",
                borderWidth: 3,
                type: 'line'
            }
        ];

        //get forecast for selected city
        this.getForecastData = () => {

            let foreCastData = [];
            let chartLabels = [];

            weatherService.getCurrentForecast(this.cityData.id).then((data) => {
                let tempList = data.data.list;

                for (i = 0; i < tempList.length; i++) {
                    if (tempList[i].dt_txt.indexOf(this.sea_level_time) > -1) {
                        foreCastData.push(tempList[i].main.sea_level);
                        chartLabels.push(weatherService.getDay(tempList[i].dt));
                    }
                }

                this.foreCastData = foreCastData;
                this.chartLabels = chartLabels;
            })
            .catch(() => {
                alert('Sorry, Something went wrong');
            });
        };

        //Load default city and its data
        this.city = this.cityList[0];
        this.getData(this.city); 
    }]);

