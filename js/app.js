angular.module('weatherApp', ["chart.js"]).config(['ChartJsProvider', function (ChartJsProvider) {
    // Configure all charts
    ChartJsProvider.setOptions({
        chartColors: ['#5ed8f0'],
    });
}]);

