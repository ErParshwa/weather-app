// serivce for ferching data from api
(function () {
	angular.module('weatherApp').
		factory('weatherService', ['$http', function ($http) {
			const baseUrl = 'http://api.openweathermap.org/data/2.5/'; //api base url
			const appId = '3d8b309701a13f65b660fa2c64cdc517'; // api key for openwethermap
			const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']; // Days list

			return {
				// - get weather data for selected city
				getCurrentWeather: function (cityId) {
					return $http.jsonp(baseUrl + '/weather?id=' + cityId + '&units=metric&mode=json&appid=' + appId + '&callback=JSON_CALLBACK');
				},

				// - get forecast for selected city
				getCurrentForecast: function (cityId) {
					return $http.jsonp(baseUrl + 'forecast?id=' + cityId + '&units=metric&mode=json&appid=' + appId + '&callback=JSON_CALLBACK');
				},

				// - get day from the timestamp				
				getDay: function (timestamp) {
					let theDate = new Date(timestamp * 1000);
					return weekDays[theDate.getUTCDay()];
				},

				// - get day and time from the timestamp
				getDayAndTime: function (timestamp) {
					let theDate = new Date(timestamp * 1000);
					return weekDays[theDate.getUTCDay()] + ', '
						+ this.getFormattedHour(theDate.getUTCHours()) + ':'
						+ this.getFormattedTime(theDate.getUTCMinutes()) + ' '
						+ this.getTimeAbbreviation(theDate.getUTCHours());
				},

				// - get formatted hour(with 12 hr format)
				getFormattedHour: function (hr) {
					hr = hr > 12 ? hr - 12 : hr;
					return hr > 9 ? hr : '0' + hr;
				},

				// - get time abbreviation
				getTimeAbbreviation: function (hr) {
					return hr > 12 ? 'PM' : 'AM';
				},

				// - get formatted time 
				getFormattedTime: function (time) {
					return time > 9 ? time : '0' + time;
				}
			}
		}])
})();